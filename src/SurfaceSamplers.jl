# Copyright (C) 2024 Dominik Itner
# SurfaceSamplers.jl is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# SurfaceSamplers.jl is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
# You should have received a copy of the GNU Lesser General Public License along with SurfaceSamplers.jl.  If not, see <http://www.gnu Lesser.org/licenses/>.
# A copy of the GNU Lesser General Public License has deliberately not been included to circumvent unnecessary traffic due to the high degree of modularity and interoperability of packages in Mosaic.jl.
# The license can instead be found at <https://codeberg.org/Mosaic/License>.

module SurfaceSamplers

# External dependencies

using LinearAlgebra

using SamplerBase.Core: Sampler
using SamplerBase.Interface: samples

import SamplerBase.Interface as SamplerInterface

# Supertype

abstract type AbstractSurfaceSampler <: Sampler end

# Implementations

"""
	SurfaceSampler

Sample only the `2n` `n-1`-dimensional surfaces of a unit `n`-cube (`[0, 1]ⁿ`) by using subsamplers provided by user.
"""
struct SurfaceSampler{T,I,N} <: AbstractSurfaceSampler
	subsamplers::T
	ndims::I
	n::N
	size::Tuple{I,I}
end

"""
	surface(subsampler::Sampler, ndims)

Create a `SurfaceSampler` repeating `subsampler` `2ndims` times. Does *not* recast the provided `subsampler` but reuse it under-the-hood.
"""
function surface(subsampler::Sampler, ndims_::Integer; gridsize=nothing)
	# assert that dimensionality of sampler is ndims_-1
	@assert ndims(subsampler) == ndims_-1
	# use sampler for all samples
	SurfaceSampler(subsampler, ndims_, gridsize, (length(subsampler), 2ndims_))
end

"""
	surface(subsamplers...)

Create a `SurfaceSampler` provided `2n` `Sampler`s where `n` is the final dimensionality of the `SurfaceSampler`.
"""
function surface(subsamplers::Vararg{Sampler}; gridsize=nothing)
	# get the number of samplers
	nsamplers = length(subsamplers)
	# assert that it is an even number of samplers
	@assert iszero(mod(nsamplers, 2))
	# compute number of ambient dimensions
	ndims_ = nsamplers÷2
	# assert that dimensionality of each sampler is ndims_-1
	@assert all(ndims(sampler) == ndims_-1 for sampler in subsamplers)
	# done
	SurfaceSampler(subsamplers, ndims_, gridsize, (length(first(subsamplers)), nsamplers))
end

"""
	surface(samplergenerator::Function, nsamples, n)

Create an `n`-dimensional `SurfaceSampler` generating `n-1`-dimensional subsamplers on-the-fly.
Be aware that `nsamples` is not the final number of samples of the `SurfaceSampler` nor must necessarily be the final number of samples per subsampler; it is merely the *input* to the `samplergenerator` function!
"""
function surface(samplergenerator::Function, nsamples, ndims_; gridsize=nothing)
	# generate 2×ndims_ subsamplers with 
	subsamplers = Tuple(samplergenerator(nsamples, ndims_-1) for _ in Base.OneTo(2ndims_))
	# done
	SurfaceSampler(subsamplers, ndims_, gridsize, (length(first(subsamplers)), 2ndims_))
end

# Extend interface

## Base: size

Base.size(sampler::SurfaceSampler) =
	sampler.size

## Base: length

Base.length(sampler::SurfaceSampler) =
	prod(size(sampler))

## Base: getindex

Base.getindex(sampler::SurfaceSampler, i::Integer) =
	# convert linear index into cartesian index to access correct subsampler and face of hypercube and access via cartesian indices
	sampler[CartesianIndices(size(sampler))[i]]

# Given a single subsampler, reuse it
Base.getindex(sampler::SurfaceSampler, c::CartesianIndex) =
	# fetch sample from subsampler and insert orientation `[0,1]` of face
	insert!(fix_cube_edges!(getsubsample(sampler, Tuple(c)...), sampler.n), (c[2]+1)÷2, mod(c[2]-1, 2))

fix_cube_edges!(subsample, ::Nothing) =
	subsample

"""
	fix_cube_edges!(subsample, n)

Translate and scale samples on the surfaces of the `n`-cube provided by a `SurfaceSampler` to fix oversampling of edges.
"""
function fix_cube_edges!(subsample, n)
	# translate to center
	broadcast!(*, subsample, subsample, 1-inv(n))
	# double size
	broadcast!(+, subsample, subsample, inv(2n))
end

getsubsample(sampler::SurfaceSampler{<:Sampler}, i, _) =
	sampler.subsamplers[i]

getsubsample(sampler::SurfaceSampler, i, j) =
	sampler.subsamplers[j][i]

## Sampler: ndims

# Number of dimensions
SamplerInterface.ndims(sampler::SurfaceSampler) =
	sampler.ndims

# Spherical sampler

"""
	SphereSampler

Sample only the `n-1`-dimensional surface of a unit `n`-sphere (`[±1]ⁿ`) by using subsamplers provided by user.
"""
struct SphereSampler{T<:SurfaceSampler,A} <: AbstractSurfaceSampler
	sampler::T
	α::A
	function SphereSampler(sampler::T, gridsize) where T
		# compute correction angle for edges
		α = 0.25π*(1-inv(gridsize))
		# done
		new{T,typeof(α)}(sampler, α)
	end
end

"""
	spherical(sampler::SurfaceSampler)

Convert a `SurfaceSampler` into a `SphereSampler` by projecting a translated `n`-cube onto the `n`-sphere.
"""
spherical(sampler::SurfaceSampler; gridsize=(length(sampler)/(2ndims(sampler)))^inv(ndims(sampler)-1)) =
	SphereSampler(sampler, ceil(Int, gridsize))

# Extend interface

## Base: size

Base.size(sphere::SphereSampler) =
	sphere.sampler.size

## Base: length

Base.length(sphere::SphereSampler) =
	prod(size(sphere))

## Base: getindex

# Adapt `SurfaceSampler` interface
Base.getindex(sphere::SphereSampler, i::Integer) =
	# convert linear index into cartesian index to access correct subsampler and face of hypercube and access via cartesian indices
	sphere[CartesianIndices(size(sphere))[i]]

# Given a single subsampler, reuse it
Base.getindex(sphere::SphereSampler, c::CartesianIndex) =
	# fetch sample from subsampler and insert orientation `[0,1]` of face
	normalize!(insert!(fix_sphere_projection!(getsubsample(sphere.sampler, Tuple(c)...), sphere.α), (c[2]+1)÷2, 2mod(c[2]-1, 2)-1))

"""
	fix_sphere_projection!(subsample, α)

In case the basis of a `SphereSampler` is the `n`-cube of a `SurfaceSampler`, fix the overlap of edges on the `n`-cube and apply a transformation of the sample points along the dimensions to fix the projection onto the `n`-sphere to preserve distances.
"""
function fix_sphere_projection!(subsample, α)
	# translate to center
	broadcast!(-, subsample, subsample, 0.5)
	# double size
	broadcast!(*, subsample, subsample, 2α)
	# transform edges to guarantee great circle distances are preserved during projection
	broadcast!(tan, subsample, subsample)
end

## Sampler: ndims

# Number of dimensions
SamplerInterface.ndims(sphere::SphereSampler) =
	sphere.sampler.ndims

# # Old Implementations

# # angularradius(nsamples, ndims) =
# # 	( √π * gamma(0.5(ndims-1))/gamma(0.5ndims) * (ndims-1)/nsamples )^inv(ndims-1)

# """
# 	projection_corrected_standard(σ, radius, x, ndims)

# Compute projection corrected standard deviation `σ` depending on the `radius` of the `n`-shpere, the sample `x` and the number dimensions `ndims`.
# (I have no idea, this must have come in a dream to me.)
# """
# projection_corrected_standard(σ, radius, x, ndims) =
# 	norm(x) / sqrt(ndims-1) * sin(σ/radius)

# projection_correction(xᵢ, radius) =
# 	√(1 + (abs(xᵢ)/radius)^2)

# "Add gaussian noise to points to be projected on an `n`-sphere."
# function addnoise!(X::AbstractMatrix, radius, σ)
# 	# get number of dimensions
# 	ndims = size(X,1)
# 	# iterate over samples
# 	for xᵢ in eachcol(X)
# 		# generate noise with projection corrected standard deviation and add onto point
# 		# xᵢ .= xᵢ .+ projection_corrected_standard(σ, radius, xᵢ, ndims) .* randn(ndims)
# 		for i in eachindex(xᵢ)
# 			xᵢ[i] = xᵢ[i] + σ * projection_correction(xᵢ[i], radius) * randn()
# 		end
# 	end
# 	# return sample matrix
# 	X
# end

end # SurfaceSamplers