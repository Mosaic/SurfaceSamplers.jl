using Test
using LinearAlgebra
using SurfaceSamplers: surface, spherical
using GridSamplers: regulargrid

# Test function for hypercube
function testcube(samples, nsamples)
	# grid squares the number of samples for two dimensions
	nsamples² = nsamples*nsamples
	# all samples at position should be either 0 or 1
	all(map(enumerate(samples)) do (i, sample)
		k = 1 + (i-1)÷nsamples²
		iszero(sample[(k+1)÷2]) || isone(sample[(k+1)÷2])
	end)
end

# Define variables for 
nsamples = 10
ndims = 3

# Automatically generate hypercube surface
sampler = surface(regulargrid, nsamples, ndims; gridsize=nsamples)

# Test composition
@test testcube(sampler, nsamples)

# Explicitly create a grid...
grid = regulargrid(nsamples, ndims-1)
# ...and repeat it automatically
sampler = surface(grid, 3; gridsize=nsamples)

# Test composition
@test testcube(sampler, nsamples)

# Explicitly use the same grid over and over...
sampler = surface((grid for _ in 1:2ndims)...; gridsize=nsamples)

# Test composition
@test testcube(sampler, nsamples)

# Transform hypercube into a hypersphere
sampler = spherical(surface(grid, 3); gridsize=nsamples)

# Test unit radius (at least approximately)
all(map(sampler) do samples
	norm(samples) ≈ 1
end)